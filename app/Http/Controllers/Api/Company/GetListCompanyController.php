<?php

namespace App\Http\Controllers\Api\Company;

use App\Http\Controllers\Controller;
use Vocces\Company\Application\CompanyLister;

class GetListCompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke(CompanyLister $service)
    {
        $company_list = $service->handle();
        return response($company_list, 201);

        //return Company::orderBy('created_at')->get();
    }

}
