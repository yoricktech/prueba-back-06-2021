<?php

namespace App\Http\Controllers\Api\Company;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Vocces\Company\Application\CompanyActivator;
use App\Http\Requests\Company\ActivateCompanyRequest;

class PutActivateCompanyController extends Controller
{
    /**
     * Change company from inactive to active
     *
     * @param \App\Http\Requests\Company\ActivateCompanyRequest $request
     */
    public function __invoke(ActivateCompanyRequest $request, CompanyActivator $service)
    {
        DB::beginTransaction();
        try {
            $company = $service->activate($request->id);
            DB::commit();
            return response($company, 201);
        } catch (\Throwable $error) {
            DB::rollback();
            $err['message'] = $error->errorInfo[2];
            return response(json_encode($err), 500);
        }
    }
}