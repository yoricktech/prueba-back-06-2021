<?php

namespace Vocces\Product\Domain;

interface ProductRepositoryInterface
{
    /**
     * Persist a new product instance
     *
     * @param Product $product
     *
     * @return void
     */
    public function create(Product $product): void;

}
