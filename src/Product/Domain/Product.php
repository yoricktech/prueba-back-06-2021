<?php

namespace Vocces\Product\Domain;

use Vocces\Product\Domain\ValueObject\ProductId;
use Vocces\Product\Domain\ValueObject\ProductName;
use Vocces\Product\Domain\ValueObject\ProductDescription;
use Vocces\Product\Domain\ValueObject\ProductImage;
use Vocces\Shared\Infrastructure\Interfaces\Arrayable;

final class Product implements Arrayable
{
    /**
     * @var \Vocces\Product\Domain\ValueObject\ProductId
     */
    private ProductId $id;

    /**
     * @var \Vocces\Product\Domain\ValueObject\ProductName
     */
    private ProductName $name;

    /**
     * @var \Vocces\Product\Domain\ValueObject\ProductDescription
     */
    private ProductDescription $description;

    /**
     * @var \Vocces\Product\Domain\ValueObject\ProductImage
     */
    private ProductImage $image;

    public function __construct(
        ProductId $id,
        ProductName $name,
        ProductDescription $description,
        ProductImage $image,
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
        $this->image = $image;
    }

    public function id(): ProductId
    {
        return $this->id;
    }

    public function name(): ProductName
    {
        return $this->name;
    }

    public function description(): ProductDescription
    {
        return $this->email;
    }

    public function image(): ProductImage
    {
        return $this->address;
    }

    public function toArray()
    {
        return [
            'id'          => $this->id()->get(),
            'name'        => $this->name()->get(),
            'description' => $this->description()->get(),
            'image'       => $this->image()->get(),
        ];
    }
}
