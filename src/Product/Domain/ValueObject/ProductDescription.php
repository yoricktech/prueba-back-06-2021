<?php

namespace Vocces\Product\Domain\ValueObject;

final class ProductDescription
{

    private string $description;

    public function __construct(string $description)
    {
        $this->description = $description;
    }

    public function get(): string
    {
        return $this->description;
    }

    public function __toString()
    {
        return $this->description;
    }
}
