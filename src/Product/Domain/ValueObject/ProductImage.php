<?php

namespace Vocces\Product\Domain\ValueObject;

final class ProductImage
{

    private string $image;

    public function __construct(string $image)
    {
        $this->image = $image;
    }

    public function get(): string
    {
        return $this->image;
    }

    public function __toString()
    {
        return $this->image;
    }
}