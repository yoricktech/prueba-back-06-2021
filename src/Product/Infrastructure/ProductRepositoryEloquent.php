<?php

namespace Vocces\Product\Infrastructure;

use App\Models\Product as ModelsProduct;
use Vocces\Product\Domain\Product;
use Vocces\Product\Domain\ProductRepositoryInterface;

class ProductRepositoryEloquent implements ProductRepositoryInterface
{
    /**
     * @inheritDoc
     */
    public function create(Product $product): void
    {
        ModelsProduct::Create([
            'id'      => $product->id(),
            'name'    => $product->name(),
            'email'   => $product->description(),
            'address' => $product->image(),
        ]);
    }

}
