<?php

namespace Vocces\Company\Infrastructure;

use App\Models\Company as ModelsCompany;
use Vocces\Company\Domain\Company;
use Vocces\Company\Domain\CompanyRepositoryInterface;

class CompanyRepositoryEloquent implements CompanyRepositoryInterface
{
    /**
     * @inheritDoc
     */
    public function create(Company $company): void
    {
        ModelsCompany::Create([
            'id'      => $company->id(),
            'name'    => $company->name(),
            'email'   => $company->email(),
            'address' => $company->address(),
            'status'  => $company->status(),
        ]);
    }

    /**
     * @inheritDoc
     */
    public function activate($id)
    {
        $company = ModelsCompany::findOrFail($id);
        $company->status = 'active';
        $company->save();
     
        return $company;
    }
    /**
     * @inheritDoc
     */
    public function get()
    {
        $list = ModelsCompany::orderBy('name')->get();
        return $list;
    }

}
